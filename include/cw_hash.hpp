#pragma once

#include "cw_meta.hpp"

#include <array>
#include <stdexcept>
#include <vector>

namespace cw::hash {
static inline constexpr auto fnv_offset_basis{14695981039346656037UL};

static inline constexpr auto fnv_prime{1099511628211UL};

using Error = std::runtime_error;

[[nodiscard]] auto fnv1(const std::uint8_t *data, std::size_t size) -> std::uint64_t;

template <typename Type, typename = meta::enable_if_any_same<Type, std::string, std::vector<std::uint8_t>>>
[[nodiscard]] inline auto fnv1(const Type &data) -> std::uint64_t {
  return fnv1(reinterpret_cast<const std::uint8_t *const>(data.data()), data.size()); // NOLINT
}

template <const std::size_t Size>
[[nodiscard]] inline auto fnv1(const std::array<std::uint8_t, Size> &data) -> std::uint64_t {
  return fnv1(reinterpret_cast<const std::uint8_t *const>(data.data()), Size); // NOLINT
}

[[nodiscard]] auto fnv1a(const std::uint8_t *data, std::size_t size) -> std::uint64_t;

template <typename Type, typename = meta::enable_if_any_same<Type, std::string, std::vector<std::uint8_t>>>
[[nodiscard]] inline auto fnv1a(const Type &data) -> std::uint64_t {
  return fnv1a(reinterpret_cast<const std::uint8_t *const>(data.data()), data.size()); // NOLINT
}

template <const std::size_t Size>
[[nodiscard]] inline auto fnv1a(const std::array<std::uint8_t, Size> &data) -> std::uint64_t {
  return fnv1a(reinterpret_cast<const std::uint8_t *const>(data.data()), Size); // NOLINT
}

[[nodiscard]] auto jenkins(const std::uint8_t *data, std::size_t size) -> std::uint64_t;

template <typename Type, typename = meta::enable_if_any_same<Type, std::string, std::vector<std::uint8_t>>>
[[nodiscard]] inline auto jenkins(const Type &data) -> std::uint64_t {
  return jenkins(reinterpret_cast<const std::uint8_t *const>(data.data()), data.size()); // NOLINT
}

template <const std::size_t Size>
[[nodiscard]] inline auto jenkins(const std::array<std::uint8_t, Size> &data) -> std::uint64_t {
  return jenkins(reinterpret_cast<const std::uint8_t *const>(data.data()), Size); // NOLINT
}
} // namespace cw::hash

namespace cw::hash::cx {
[[nodiscard]] constexpr auto fnv1(const std::uint8_t *const data, const std::size_t size) -> std::uint64_t {
  if (data == nullptr) {
    throw Error("Parameter data is null.");
  }

  auto hash{fnv_offset_basis};

  for (auto i{0UL}; i < size; ++i) {
    hash *= fnv_prime;

    hash ^= static_cast<std::uint64_t>(data[i]); // NOLINT
  }

  return hash;
}

template <const std::size_t Size>
[[nodiscard]] inline auto fnv1(const std::array<std::uint8_t, Size> &data) -> std::uint64_t {
  return fnv1(reinterpret_cast<const std::uint8_t *const>(data.data()), Size); // NOLINT
}

[[nodiscard]] constexpr auto fnv1a(const std::uint8_t *const data, const std::size_t size) -> std::uint64_t {
  if (data == nullptr) {
    throw Error("Parameter data is null.");
  }

  auto hash{fnv_offset_basis};

  for (auto i{0UL}; i < size; ++i) {
    hash ^= static_cast<std::uint64_t>(data[i]); // NOLINT

    hash *= fnv_prime;
  }

  return hash;
}

template <const std::size_t Size>
[[nodiscard]] inline auto fnv1a(const std::array<std::uint8_t, Size> &data) -> std::uint64_t {
  return fnv1a(reinterpret_cast<const std::uint8_t *const>(data.data()), Size); // NOLINT
}

[[nodiscard]] constexpr auto jenkins(const std::uint8_t *const data, const std::size_t size) -> std::uint64_t {
  if (data == nullptr) {
    throw Error("Parameter data is null.");
  }

  auto hash{0UL};

  for (auto i{0UL}; i < size; ++i) {
    hash += static_cast<std::uint64_t>(data[i]); // NOLINT

    hash += hash << 10; // NOLINT

    hash ^= hash >> 6; // NOLINT
  }

  hash += hash << 3; // NOLINT

  hash ^= hash >> 11; // NOLINT

  hash += hash << 15; // NOLINT

  return hash;
}

template <const std::size_t Size>
[[nodiscard]] inline auto jenkins(const std::array<std::uint8_t, Size> &data) -> std::uint64_t {
  return jenkins(reinterpret_cast<const std::uint8_t *const>(data.data()), Size); // NOLINT
}
} // namespace cw::hash::cx
