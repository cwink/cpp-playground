#pragma once

#include "cw_meta.hpp"
#include "cw_stream.hpp"

#include <fstream>
#include <memory>
#include <unordered_map>
#include <variant>
#include <vector>

namespace cw::json {
static constexpr auto default_write_buffer_size{512};

enum class Type : std::uint8_t { object, array, string, number, boolean, null };

auto operator<<(std::ostream &os, Type type) -> std::ostream &;

class Error final : public std::runtime_error {
public:
  Error() = delete;

  explicit Error(const std::string &message);

  Error(const stream::Location &location, const std::string &message);

  Error(const stream::Location &location, char symbol);

  Error(Type expected, Type received);

  Error(const stream::Location &location, char expected, char received);

  Error(const Error &error) = default;

  Error(Error &&error) noexcept = default;

  [[nodiscard]] auto operator=(const Error &error) -> Error & = default;

  [[nodiscard]] auto operator=(Error &&error) noexcept -> Error & = default;

  ~Error() override;
};

class Value;

using Unique_value = std::unique_ptr<Value>;

using Key = std::string;

using Object = std::unordered_map<Key, Unique_value>;

using Array = std::vector<Value>;

using String = std::string;

using Number = double;

using Boolean = bool;

class Value final {
  static constexpr auto padding_size_{7};

  using Data = std::variant<Object, Array, String, Number, Boolean>;

public:
  Value() = default;

  explicit Value(const Object &value);

  explicit Value(Object &&value);

  explicit Value(const Array &value);

  explicit Value(Array &&value);

  explicit Value(const String &value);

  explicit Value(String &&value);

  explicit Value(Number value);

  explicit Value(Boolean value);

  Value(const Value &value);

  Value(Value &&value) noexcept = default;

  [[nodiscard]] auto operator=(const Value &value) -> Value &;

  [[nodiscard]] auto operator=(Value &&value) noexcept -> Value & = default;

  ~Value() = default;

  [[nodiscard]] auto type() const noexcept -> Type;

  [[nodiscard]] auto object() const -> const Object &;

  [[nodiscard]] auto object() -> Object &;

  [[nodiscard]] auto array() const -> const Array &;

  [[nodiscard]] auto array() -> Array &;

  [[nodiscard]] auto string() const -> const String &;

  [[nodiscard]] auto string() -> String &;

  [[nodiscard]] auto number() const -> Number;

  [[nodiscard]] auto boolean() const -> Boolean;

  auto object(const Object &value) -> void;

  auto object(Object &&value) -> void;

  auto array(const Array &value) -> void;

  auto array(Array &&value) -> void;

  auto string(const String &value) -> void;

  auto string(String &&value) -> void;

  auto number(Number value) -> void;

  auto boolean(Boolean value) -> void;

  auto null() -> void;

  [[nodiscard]] auto find(const std::string &key) const -> std::vector<std::reference_wrapper<const Value>>;

  [[nodiscard]] auto find(const std::string &key) -> std::vector<std::reference_wrapper<Value>>;

  [[nodiscard]] auto to_string() const -> std::string;

  [[nodiscard]] auto to_string_formatted(std::size_t indent_size = 2) const -> std::string;

  auto write_to(std::ostream &os) const -> void;

  auto write_to_formatted(std::ostream &os, std::size_t indent_size = 2) const -> void;

  [[nodiscard]] auto operator[](const std::string &key) -> Value &;

  [[nodiscard]] auto operator[](std::string &&key) -> Value &;

  auto operator=(const Object &value) -> Value &;

  auto operator=(Object &&value) -> Value &;

  auto operator=(const Array &value) -> Value &;

  auto operator=(Array &&value) -> Value &;

  auto operator=(const String &value) -> Value &;

  auto operator=(String &&value) -> Value &;

  auto operator=(Number value) -> Value &;

  auto operator=(Boolean value) -> Value &;

  auto operator=(const char *value) -> Value &;

private:
  Data data_ = Data();

  Type type_ = Type::null;

  auto write_to_(std::ostream &os) const -> void;

  auto write_to_formatted_(std::ostream &os, std::size_t current_indent = 0, std::size_t indent_size = 2) const -> void;

  [[maybe_unused]] std::array<std::uint8_t, padding_size_> padding_{0};
};

[[nodiscard]] auto operator==(const Value &left, const Value &right) -> bool;

[[nodiscard]] auto operator!=(const Value &left, const Value &right) -> bool;

auto operator<<(std::ostream &os, const Value &value) -> std::ostream &;
} // namespace cw::json

namespace cw::json::parse {
class Parser {
  static constexpr auto default_object_reserve_{16};

  static constexpr auto default_array_reserve_{8};

  static constexpr auto default_string_reserve_{64};

public:
  Parser(const Parser &parser) = delete;

  Parser(Parser &&parser) noexcept = default;

  [[nodiscard]] auto operator=(const Parser &parser) -> Parser & = delete;

  [[nodiscard]] auto operator=(Parser &&parser) noexcept -> Parser & = default;

  virtual ~Parser();

  [[nodiscard]] auto object_reserve(std::size_t size) noexcept -> Parser &;

  [[nodiscard]] auto array_reserve(std::size_t size) noexcept -> Parser &;

  [[nodiscard]] auto string_reserve(std::size_t size) noexcept -> Parser &;

  [[nodiscard]] virtual auto parse() -> Value = 0;

protected:
  Parser() = default;

  [[nodiscard]] auto stream__() const -> const std::unique_ptr<std::istream> &;

  [[nodiscard]] auto stream__() -> std::unique_ptr<std::istream> &;

  auto parse_whitespace_() -> void;

  [[nodiscard]] auto parse_value_() -> Value;

private:
  std::unique_ptr<std::istream> stream_ = std::unique_ptr<std::istream>();

  std::size_t object_reserve_{default_object_reserve_};

  std::size_t array_reserve_{default_array_reserve_};

  std::size_t string_reserve_{default_string_reserve_};

  [[nodiscard]] auto parse_key_() -> std::string;

  [[nodiscard]] auto parse_object_() -> Value;

  [[nodiscard]] auto parse_array_() -> Value;

  [[nodiscard]] auto parse_string_() -> Value;

  [[nodiscard]] auto parse_number_() -> Value;

  [[nodiscard]] auto parse_boolean_() -> Value;

  [[nodiscard]] auto parse_null_() -> Value;
};

class File final : public Parser {
  static constexpr auto default_buffer_size_{512};

public:
  File() = delete;

  explicit File(std::string filename);

  File(const File &file) = delete;

  File(File &&file) noexcept = default;

  [[nodiscard]] auto operator=(const File &file) -> File & = delete;

  [[nodiscard]] auto operator=(File &&file) -> File & = default;

  ~File() override;

  [[nodiscard]] auto buffer_size(std::size_t size) noexcept -> File &;

  [[nodiscard]] auto parse() -> Value override;

private:
  std::string filename_ = std::string();

  std::size_t buffer_size_{default_buffer_size_};
};

class String final : public Parser {
public:
  String() = delete;

  explicit String(const std::string &value);

  String(const String &file) = delete;

  String(String &&file) noexcept = default;

  [[nodiscard]] auto operator=(const String &file) -> String & = delete;

  [[nodiscard]] auto operator=(String &&file) -> String & = default;

  ~String() override;

  [[nodiscard]] auto parse() -> Value override;
};
} // namespace cw::json::parse

namespace cw::json::writer {
class File final {
  static constexpr auto default_buffer_size_{512};

  static constexpr auto padding_size_{7};

public:
  File() = delete;

  explicit File(std::string filename);

  File(const File &writer) = delete;

  File(File &&writer) noexcept = default;

  [[nodiscard]] auto operator=(const File &writer) -> File & = delete;

  [[nodiscard]] auto operator=(File &&writer) noexcept -> File & = default;

  ~File() = default;

  [[nodiscard]] auto buffer_size(std::size_t size) noexcept -> File &;

  [[nodiscard]] auto formatted(bool value) noexcept -> File &;

  auto write(const Value &value) const -> void;

private:
  std::string filename_ = std::string();

  std::size_t buffer_size_{default_buffer_size_};

  bool formatted_{false};

  [[maybe_unused]] std::array<std::uint8_t, padding_size_> padding_{0};
};
} // namespace cw::json::writer

namespace cw::json::builder {
class Object final {
public:
  Object() = default;

  template <typename Type, typename = meta::enable_if_any_same<Type, json::Object, json::Array, String>>
  Object(const std::string &key, const Type &value) {
    if constexpr (std::is_same_v<Type, json::Object>) {
      auto object = json::Object();

      for (const auto &entry : value) {
        object[entry.first] = std::make_unique<Value>(*entry.second);
      }

      this->object_.emplace(std::make_pair(key, std::make_unique<Value>(Value(std::move(object)))));
    } else {
      this->object_.emplace(std::make_pair(key, std::make_unique<Value>(Value(value))));
    }
  }

  template <typename Type, typename = meta::enable_if_any_same<Type, json::Object, json::Array, String>>
  Object(const std::string &key, Type &&value) {
    this->object_.emplace(std::make_pair(key, std::make_unique<Value>(Value(std::forward<Type>(value)))));
  }

  template <typename Type, typename = meta::enable_if_any_same<Type, json::Object, json::Array, String>>
  Object(std::string &&key, Type &&value) {
    this->object_.emplace(std::make_pair(std::move(key), std::make_unique<Value>(Value(std::forward<Type>(value)))));
  }

  template <typename Type, typename = std::enable_if_t<meta::is_any_same_v<Type, Number, Boolean> ||
                                                       meta::is_fixed_width_integer<Type>>>
  Object(const std::string &key, const Type value) {
    if constexpr (meta::is_fixed_width_integer<Type>) {
      this->object_.emplace(std::make_pair(key, std::make_unique<Value>(Value(static_cast<Number>(value)))));
    } else {
      this->object_.emplace(std::make_pair(key, std::make_unique<Value>(Value(value))));
    }
  }

  template <typename Type, typename = std::enable_if_t<meta::is_any_same_v<Type, Number, Boolean> ||
                                                       meta::is_fixed_width_integer<Type>>>
  Object(std::string &&key, const Type value) {
    if constexpr (meta::is_fixed_width_integer<Type>) {
      this->object_.emplace(std::make_pair(std::move(key), std::make_unique<Value>(Value(static_cast<Number>(value)))));
    } else {
      this->object_.emplace(std::make_pair(std::move(key), std::make_unique<Value>(Value(value))));
    }
  }

  template <typename Type, typename = std::enable_if_t<meta::is_any_same_v<Type, Number, Boolean> ||
                                                       meta::is_fixed_width_integer<Type>>>
  Object(const char *const key, const Type value) {
    if constexpr (meta::is_fixed_width_integer<Type>) {
      this->object_.emplace(
          std::make_pair(std::string(key), std::make_unique<Value>(Value(static_cast<Number>(value)))));
    } else {
      this->object_.emplace(std::make_pair(std::string(key), std::make_unique<Value>(Value(value))));
    }
  }

  template <typename Type, typename = meta::enable_if_any_same<Type, json::Object, json::Array, String>>
  Object(const char *const key, const Type &value) { // NOLINT(readability-avoid-const-params-in-decls)
    if constexpr (std::is_same_v<Type, json::Object>) {
      auto object = json::Object();

      for (const auto &entry : value) {
        object[entry.first] = std::make_unique<Value>(*entry.second);
      }

      this->object_.emplace(std::make_pair(std::string(key), std::make_unique<Value>(Value(std::move(object)))));
    } else {
      this->object_.emplace(std::make_pair(std::string(key), std::make_unique<Value>(Value(value))));
    }
  }

  template <typename Type, typename = meta::enable_if_any_same<Type, json::Object, json::Array, String>>
  Object(const char *const key, Type &&value) {
    this->object_.emplace(std::make_pair(std::string(key), std::make_unique<Value>(Value(std::forward<Type>(value)))));
  }

  explicit Object(const std::string &key);

  explicit Object(std::string &&key);

  explicit Object(const char *key);

  Object(const std::string &key, const char *value);

  Object(std::string &&key, const char *value);

  Object(const char *key, const char *value);

  Object(const Object &object);

  Object(Object &&object) noexcept = default;

  [[nodiscard]] auto operator=(const Object &object) -> Object &;

  [[nodiscard]] auto operator=(Object &&object) noexcept -> Object & = default;

  ~Object() = default;

  template <typename Type, typename = meta::enable_if_any_same<Type, json::Object, json::Array, String>>
  [[nodiscard]] auto add(const std::string &key, const Type &value) -> Object & {
    if constexpr (std::is_same_v<Type, json::Object>) {
      auto object = json::Object();

      for (const auto &entry : value) {
        object[entry.first] = std::make_unique<Value>(*entry.second);
      }

      this->object_.emplace(std::make_pair(key, std::make_unique<Value>(Value(std::move(object)))));
    } else {
      this->object_.emplace(std::make_pair(key, std::make_unique<Value>(Value(value))));
    }

    return *this;
  }

  template <typename Type, typename = meta::enable_if_any_same<Type, json::Object, json::Array, String>>
  [[nodiscard]] auto add(const std::string &key, Type &&value) -> Object & {
    this->object_.emplace(std::make_pair(key, std::make_unique<Value>(Value(std::forward<Type>(value)))));

    return *this;
  }

  template <typename Type, typename = meta::enable_if_any_same<Type, json::Object, json::Array, String>>
  [[nodiscard]] auto add(std::string &&key, Type &&value) -> Object & {
    this->object_.emplace(std::make_pair(std::move(key), std::make_unique<Value>(Value(std::forward<Type>(value)))));

    return *this;
  }

  template <typename Type, typename = std::enable_if_t<meta::is_any_same_v<Type, Number, Boolean> ||
                                                       meta::is_fixed_width_integer<Type>>>
  [[nodiscard]] auto add(const std::string &key, const Type value) -> Object & { // NOLINT
    if constexpr (meta::is_fixed_width_integer<Type>) {
      this->object_.emplace(std::make_pair(key, std::make_unique<Value>(Value(static_cast<Number>(value)))));
    } else {
      this->object_.emplace(std::make_pair(key, std::make_unique<Value>(Value(value))));
    }

    return *this;
  }

  template <typename Type, typename = std::enable_if_t<meta::is_any_same_v<Type, Number, Boolean> ||
                                                       meta::is_fixed_width_integer<Type>>>
  [[nodiscard]] auto add(std::string &&key, const Type value) -> Object & { // NOLINT
    if constexpr (meta::is_fixed_width_integer<Type>) {
      this->object_.emplace(std::make_pair(std::move(key), std::make_unique<Value>(Value(static_cast<Number>(value)))));
    } else {
      this->object_.emplace(std::make_pair(std::move(key), std::make_unique<Value>(Value(value))));
    }

    return *this;
  }

  template <typename Type, typename = std::enable_if_t<meta::is_any_same_v<Type, Number, Boolean> ||
                                                       meta::is_fixed_width_integer<Type>>>
  [[nodiscard]] auto add(const char *const key, const Type value) -> Object & {
    if constexpr (meta::is_fixed_width_integer<Type>) { // NOLINT
      this->object_.emplace(
          std::make_pair(std::string(key), std::make_unique<Value>(Value(static_cast<Number>(value)))));
    } else { // NOLINT
      this->object_.emplace(std::make_pair(std::string(key), std::make_unique<Value>(Value(value))));
    }

    return *this;
  }

  template <typename Type, typename = meta::enable_if_any_same<Type, json::Object, json::Array, String>>
  [[nodiscard]] auto add(const char *const key, const Type &value) -> Object & { // NOLINT
    if constexpr (std::is_same_v<Type, json::Object>) {
      auto object = json::Object();

      for (const auto &entry : value) {
        object[entry.first] = std::make_unique<Value>(*entry.second);
      }

      this->object_.emplace(std::make_pair(std::string(key), std::make_unique<Value>(Value(std::move(object)))));
    } else {
      this->object_.emplace(std::make_pair(std::string(key), std::make_unique<Value>(Value(value))));
    }

    return *this;
  }

  template <typename Type, typename = meta::enable_if_any_same<Type, json::Object, json::Array, String>>
  [[nodiscard]] auto add(const char *const key, Type &&value) -> Object & {
    this->object_.emplace(std::make_pair(std::string(key), std::make_unique<Value>(Value(std::forward<Type>(value)))));

    return *this;
  }

  [[nodiscard]] auto add(const std::string &key, const char *value) -> Object &;

  [[nodiscard]] auto add(std::string &&key, const char *value) -> Object &;

  [[nodiscard]] auto add(const char *key, const char *value) -> Object &;

  [[nodiscard]] auto add(const std::string &key) -> Object &;

  [[nodiscard]] auto add(std::string &&key) -> Object &;

  [[nodiscard]] auto add(const char *key) -> Object &;

  [[nodiscard]] auto build() -> json::Object;

private:
  json::Object object_ = json::Object();
};

class Array final {
public:
  Array() = default;

  template <typename Type, typename = meta::enable_if_any_same<Type, json::Object, json::Array, String>>
  explicit Array(const Type &value) {
    if constexpr (std::is_same_v<Type, json::Object>) {
      auto object = json::Object();

      for (const auto &entry : value) {
        object[entry.first] = std::make_unique<Value>(*entry.second);
      }

      this->array_.emplace_back(Value(std::move(object)));
    } else {
      this->array_.emplace_back(Value(value));
    }
  }

  template <typename Type, typename = meta::enable_if_any_same<Type, json::Object, json::Array, String>>
  explicit Array(Type &&value) {
    this->array_.emplace_back(Value(std::forward<Type>(value)));
  }

  template <typename Type, typename = std::enable_if_t<meta::is_any_same_v<Type, Number, Boolean> ||
                                                       meta::is_fixed_width_integer<Type>>>
  explicit Array(const Type value) {
    if constexpr (meta::is_fixed_width_integer<Type>) { // NOLINT
      this->array_.emplace_back(Value(static_cast<Number>(value)));
    } else { // NOLINT
      this->array_.emplace_back(Value(value));
    }
  }

  explicit Array(const char *value);

  Array(const Array &array) = default;

  Array(Array &&array) noexcept = default;

  [[nodiscard]] auto operator=(const Array &array) -> Array & = default;

  [[nodiscard]] auto operator=(Array &&array) noexcept -> Array & = default;

  template <typename Type, typename = meta::enable_if_any_same<Type, json::Object, json::Array, String>>
  [[nodiscard]] auto add(const Type &value) -> Array & {
    if constexpr (std::is_same_v<Type, json::Object>) {
      auto object = json::Object();

      for (const auto &entry : value) {
        object[entry.first] = std::make_unique<Value>(*entry.second);
      }

      this->array_.emplace_back(Value(std::move(object)));
    } else {
      this->array_.emplace_back(Value(value));
    }

    return *this;
  }

  template <typename Type, typename = meta::enable_if_any_same<Type, json::Object, json::Array, String>>
  [[nodiscard]] auto add(Type &&value) -> Array & {
    this->array_.emplace_back(Value(std::forward<Type>(value)));

    return *this;
  }

  template <typename Type, typename = std::enable_if_t<meta::is_any_same_v<Type, Number, Boolean> ||
                                                       meta::is_fixed_width_integer<Type>>>
  [[nodiscard]] auto add(const Type value) -> Array & {
    if constexpr (meta::is_fixed_width_integer<Type>) { // NOLINT
      this->array_.emplace_back(Value(static_cast<Number>(value)));
    } else { // NOLINT
      this->array_.emplace_back(Value(value));
    }

    return *this;
  }

  [[nodiscard]] auto add(const char *value) -> Array &;

  [[nodiscard]] auto add() -> Array &;

  [[nodiscard]] auto build() -> json::Array;

  ~Array() = default;

private:
  json::Array array_ = json::Array();
};
} // namespace cw::json::builder
