#pragma once

#include <cmath>

namespace cw::math {
template <typename Number, typename = std::enable_if_t<std::is_arithmetic_v<Number>>>
[[nodiscard]] auto equals(const Number left, const Number right) -> bool {
  static constexpr auto equals_epsilon{0.00001};

  if constexpr (std::is_floating_point_v<Number>) {
    return std::abs(left - right) < equals_epsilon;
  } else {
    return left == right;
  }
}
} // namespace cw::math
