#pragma once

#include <cstdint>
#include <type_traits>

namespace cw::meta {
template <typename First, typename Second> using enable_if_same = std::enable_if_t<std::is_same_v<First, Second>>;

template <typename First, typename... Rest> struct is_any_same final {
  static inline constexpr auto value = (std::is_same_v<First, Rest> || ...);
};

template <typename First, typename... Rest>
static inline constexpr auto is_any_same_v = (is_any_same<First, Rest...>::value);

template <typename First, typename... Rest> using enable_if_any_same = std::enable_if_t<is_any_same_v<First, Rest...>>;

template <typename Type>
static inline constexpr auto is_fixed_width_integer =
    is_any_same_v<Type, std::uint8_t, std::int8_t, std::uint16_t, std::int16_t, std::uint32_t, std::int32_t,
                  std::uint64_t, std::int64_t>;
} // namespace cw::meta
