#pragma once

#include <iostream>

namespace cw::stream {
using Location = std::pair<std::size_t, std::size_t>;

auto write_spaces_to(std::ostream &os, std::size_t count) -> void;

[[nodiscard]] auto line_and_column(std::istream &is) -> Location;
} // namespace cw::stream
