#pragma once

#include "cw_meta.hpp"

#include <sstream>

namespace cw::string {
class Builder final {
public:
  Builder();

  template <typename Type> explicit Builder(const Type &value) {
    this->oss_.exceptions(std::ostringstream::failbit | std::ostringstream::badbit);

    this->oss_ << value;
  }

  template <typename Type, typename = meta::enable_if_same<Type, Builder>> explicit Builder(Type &&value) {
    this->oss_.exceptions(std::ostringstream::failbit | std::ostringstream::badbit);

    this->oss_ << std::forward<Type>(value);
  }

  explicit Builder(const char *c_string);

  Builder(const Builder &builder);

  Builder(Builder &&) noexcept = default;

  [[nodiscard]] auto operator=(const Builder &builder) -> Builder &;

  [[nodiscard]] auto operator=(Builder &&) noexcept -> Builder & = default;

  ~Builder() = default;

  template <typename Type>[[nodiscard]] auto append(const Type &value) -> Builder & {
    this->oss_ << value;

    return *this;
  }

  template <typename Type>[[nodiscard]] auto append(Type &&value) -> Builder & {
    this->oss_ << std::forward<Type>(value);

    return *this;
  }

  [[nodiscard]] auto append(const char *c_string) -> Builder &;

  template <typename Type>[[nodiscard]] auto prepend(const Type &value) -> Builder & {
    auto temp = std::ostringstream();

    temp << value << this->oss_.rdbuf();

    this->oss_ = std::move(temp);

    return *this;
  }

  template <typename Type>[[nodiscard]] auto prepend(Type &&value) -> Builder & {
    auto temp = std::ostringstream();

    temp << std::forward<Type>(value) << this->oss_.rdbuf();

    this->oss_ = std::move(temp);

    return *this;
  }

  [[nodiscard]] auto prepend(const char *c_string) -> Builder &;

  [[nodiscard]] auto build() -> std::string;

private:
  std::ostringstream oss_ = std::ostringstream();
};
} // namespace cw::string

