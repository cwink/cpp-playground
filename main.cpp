#include "include/cw_hash.hpp"
#include "include/cw_json.hpp"

#include <iostream>

auto main([[maybe_unused]] int argc, [[maybe_unused]] char *argv[]) -> int {
  const std::uint8_t *const a = reinterpret_cast<const std::uint8_t *const>("This is a test");

  const auto b = std::string("This is a test");

  const auto c = std::vector<std::uint8_t>({'T', 'h', 'i', 's', ' ', 'i', 's', ' ', 'a', ' ', 't', 'e', 's', 't'});

  const auto d = std::array<std::uint8_t, 14>{'T', 'h', 'i', 's', ' ', 'i', 's', ' ', 'a', ' ', 't', 'e', 's', 't'};

  std::cout << cw::hash::cx::jenkins(a, 14) << std::endl;

  std::cout << cw::hash::jenkins(b) << std::endl;

  std::cout << cw::hash::jenkins(c) << std::endl;

  std::cout << cw::hash::cx::jenkins(d) << std::endl;

  return 0;
}
