#include "../include/cw_hash.hpp"

namespace cw::hash {
auto fnv1(const std::uint8_t *const data, const std::size_t size) -> std::uint64_t {
  if (data == nullptr) {
    throw Error("Parameter data is null.");
  }

  auto hash{fnv_offset_basis};

  for (auto i{0UL}; i < size; ++i) {
    hash *= fnv_prime;

    hash ^= static_cast<std::uint64_t>(data[i]); // NOLINT
  }

  return hash;
}

auto fnv1a(const std::uint8_t *const data, const std::size_t size) -> std::uint64_t {
  if (data == nullptr) {
    throw Error("Parameter data is null.");
  }

  auto hash{fnv_offset_basis};

  for (auto i{0UL}; i < size; ++i) {
    hash ^= static_cast<std::uint64_t>(data[i]); // NOLINT

    hash *= fnv_prime;
  }

  return hash;
}

auto jenkins(const std::uint8_t *const data, const std::size_t size) -> std::uint64_t {
  if (data == nullptr) {
    throw Error("Parameter data is null.");
  }

  auto hash{0UL};

  for (auto i{0UL}; i < size; ++i) {
    hash += static_cast<std::uint64_t>(data[i]); // NOLINT

    hash += hash << 10; // NOLINT

    hash ^= hash >> 6; // NOLINT
  }

  hash += hash << 3; // NOLINT

  hash ^= hash >> 11; // NOLINT

  hash += hash << 15; // NOLINT

  return hash;
}
} // namespace cw::hash
