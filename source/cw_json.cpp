#include "../include/cw_json.hpp"
#include "../include/cw_math.hpp"
#include "../include/cw_string.hpp"

#include <iomanip>

namespace cw::json {
auto operator<<(std::ostream &os, const Type type) -> std::ostream & {
  switch (type) {
  case Type::object:
    os << "object";

    break;
  case Type::array:
    os << "array";

    break;
  case Type::string:
    os << "string";

    break;
  case Type::number:
    os << "number";

    break;
  case Type::boolean:
    os << "boolean";

    break;
  case Type::null:
    os << "null";

    break;
  }

  return os;
}

Error::Error(const std::string &message) : std::runtime_error(message) {}

Error::Error(const stream::Location &location, const std::string &message)
    : std::runtime_error(string::Builder('[')
                             .append(location.first)
                             .append(':')
                             .append(location.second)
                             .append("] ")
                             .append(message)
                             .build()) {}

Error::Error(const stream::Location &location, const char symbol)
    : std::runtime_error(string::Builder('[')
                             .append(location.first)
                             .append(':')
                             .append(location.second)
                             .append("] ")
                             .append("Invalid symbol '")
                             .append(symbol)
                             .append("' found.")
                             .build()) {}

Error::Error(const Type expected, const Type received)
    : std::runtime_error(string::Builder("Expected value type '")
                             .append(expected)
                             .append("', but received value type '")
                             .append(received)
                             .append("'.")
                             .build()) {}

Error::Error(const stream::Location &location, const char expected, const char received)
    : std::runtime_error(string::Builder('[')
                             .append(location.first)
                             .append(':')
                             .append(location.second)
                             .append("] ")
                             .append("Expected symbol '")
                             .append(expected)
                             .append("', but received symbol '")
                             .append(received)
                             .append("'.")
                             .build()) {}

Error::~Error() = default;

Value::Value(const Object &value) {
  auto object = Object();

  for (const auto &entry : value) {
    object[entry.first] = std::make_unique<Value>(*entry.second);
  }

  this->data_ = std::move(object);

  this->type_ = Type::object;
}

Value::Value(Object &&value) : data_(std::move(value)), type_{Type::object} {}

Value::Value(const Array &value) : data_(value), type_{Type::array} {}

Value::Value(Array &&value) : data_(std::move(value)), type_{Type::array} {}

Value::Value(const String &value) : data_(value), type_{Type::string} {}

Value::Value(String &&value) : data_(std::move(value)), type_{Type::string} {}

Value::Value(const Number value) : data_(value), type_{Type::number} {}

Value::Value(const Boolean value) : data_(value), type_{Type::boolean} {}

Value::Value(const Value &value) {
  switch (value.type_) {
  case Type::object: {
    auto object = Object();

    for (const auto &entry : std::get<Object>(value.data_)) {
      object[entry.first] = std::make_unique<Value>(*entry.second);
    }

    this->data_ = std::move(object);
  } break;
  case Type::array:
    this->data_ = std::get<Array>(value.data_);

    break;
  case Type::string:
    this->data_ = std::get<String>(value.data_);

    break;
  case Type::number:
    this->data_ = std::get<Number>(value.data_);

    break;
  case Type::boolean:
    this->data_ = std::get<Boolean>(value.data_);

    break;
  case Type::null:
    break;
  }

  this->type_ = value.type_;
}

auto Value::operator=(const Value &value) -> Value & {
  if (this == &value) {
    return *this;
  }

  switch (value.type_) {
  case Type::object: {
    auto object = Object();

    for (const auto &entry : std::get<Object>(value.data_)) {
      object[entry.first] = std::make_unique<Value>(*entry.second);
    }

    this->data_ = std::move(object);
  } break;
  case Type::array:
    this->data_ = std::get<Array>(value.data_);

    break;
  case Type::string:
    this->data_ = std::get<String>(value.data_);

    break;
  case Type::number:
    this->data_ = std::get<Number>(value.data_);

    break;
  case Type::boolean:
    this->data_ = std::get<Boolean>(value.data_);

    break;
  case Type::null:
    break;
  }

  this->type_ = value.type_;

  return *this;
}

auto Value::type() const noexcept -> Type { return this->type_; }

auto Value::object() const -> const Object & {
  if (this->type_ != Type::object) {
    throw Error(Type::object, this->type_);
  }

  return std::get<Object>(this->data_);
}

auto Value::object() -> Object & {
  if (this->type_ != Type::object) {
    throw Error(Type::object, this->type_);
  }

  return std::get<Object>(this->data_);
}

auto Value::array() const -> const Array & {
  if (this->type_ != Type::array) {
    throw Error(Type::array, this->type_);
  }

  return std::get<Array>(this->data_);
}

auto Value::array() -> Array & {
  if (this->type_ != Type::array) {
    throw Error(Type::array, this->type_);
  }

  return std::get<Array>(this->data_);
}

auto Value::string() const -> const String & {
  if (this->type_ != Type::string) {
    throw Error(Type::string, this->type_);
  }

  return std::get<String>(this->data_);
}

auto Value::string() -> String & {
  if (this->type_ != Type::string) {
    throw Error(Type::string, this->type_);
  }

  return std::get<String>(this->data_);
}

auto Value::number() const -> Number {
  if (this->type_ != Type::number) {
    throw Error(Type::number, this->type_);
  }

  return std::get<Number>(this->data_);
}

auto Value::boolean() const -> Boolean {
  if (this->type_ != Type::boolean) {
    throw Error(Type::boolean, this->type_);
  }

  return std::get<Boolean>(this->data_);
}

auto Value::object(const Object &value) -> void {
  auto object = Object();

  for (const auto &entry : value) {
    object[entry.first] = std::make_unique<Value>(*entry.second);
  }

  this->data_ = std::move(object);

  this->type_ = Type::object;
}

auto Value::object(Object &&value) -> void {
  this->data_ = std::move(value);

  this->type_ = Type::object;
}

auto Value::array(const Array &value) -> void {
  this->data_ = value;

  this->type_ = Type::array;
}

auto Value::array(Array &&value) -> void {
  this->data_ = std::move(value);

  this->type_ = Type::array;
}

auto Value::string(const String &value) -> void {
  this->data_ = value;

  this->type_ = Type::string;
}

auto Value::string(String &&value) -> void {
  this->data_ = std::move(value);

  this->type_ = Type::string;
}

auto Value::number(const Number value) -> void {
  this->data_ = value;

  this->type_ = Type::number;
}

auto Value::boolean(const Boolean value) -> void {
  this->data_ = value;

  this->type_ = Type::boolean;
}

auto Value::null() -> void {
  this->data_ = Data();

  this->type_ = Type::null;
}

auto Value::find(const std::string &key) const -> std::vector<std::reference_wrapper<const Value>> {
  auto vec = std::vector<std::reference_wrapper<const Value>>();

  for (const auto &entry : this->object()) {
    if (entry.first == key) {
      vec.emplace_back(*entry.second);
    }

    if (entry.second->type() == Type::object) {
      for (const auto &value : entry.second->find(key)) {
        vec.emplace_back(value);
      }
    }
  }

  return vec;
}

auto Value::find(const std::string &key) -> std::vector<std::reference_wrapper<Value>> {
  auto vec = std::vector<std::reference_wrapper<Value>>();

  for (auto &entry : this->object()) {
    if (entry.first == key) {
      vec.emplace_back(*entry.second);
    }

    if (entry.second->type() == Type::object) {
      for (auto &value : entry.second->find(key)) {
        vec.emplace_back(value);
      }
    }
  }

  return vec;
}

auto Value::to_string() const -> std::string {
  auto oss = std::ostringstream();

  this->write_to(oss);

  return oss.str();
}

auto Value::to_string_formatted(const std::size_t indent_size) const -> std::string {
  auto oss = std::ostringstream();

  this->write_to_formatted(oss, indent_size);

  return oss.str();
}

auto Value::write_to(std::ostream &os) const -> void {
  this->write_to_(os);

  os.flush();
}

auto Value::write_to_formatted(std::ostream &os, const std::size_t indent_size) const -> void {
  this->write_to_formatted_(os, 0, indent_size);

  os.flush();
}

auto Value::operator[](const std::string &key) -> Value & { return *this->object()[key]; }

auto Value::operator[](std::string &&key) -> Value & { return *this->object()[std::move(key)]; }

auto Value::operator=(const Object &value) -> Value & {
  this->object(value);

  return *this;
}

auto Value::operator=(Object &&value) -> Value & {
  this->object(std::move(value));

  return *this;
}

auto Value::operator=(const Array &value) -> Value & {
  this->array(value);

  return *this;
}

auto Value::operator=(Array &&value) -> Value & {
  this->array(std::move(value));

  return *this;
}

auto Value::operator=(const String &value) -> Value & {
  this->string(value);

  return *this;
}

auto Value::operator=(String &&value) -> Value & {
  this->string(std::move(value));

  return *this;
}

auto Value::operator=(Number value) -> Value & {
  this->number(value);

  return *this;
}

auto Value::operator=(Boolean value) -> Value & {
  this->boolean(value);

  return *this;
}

auto Value::operator=(const char *const value) -> Value & {
  this->string(value);

  return *this;
}

auto Value::write_to_(std::ostream &os) const -> void {
  switch (this->type()) {
  case Type::object:
    os << '{';

    if (!this->object().empty()) {
      auto it = std::cbegin(this->object());

      os << '"' << it->first << "\":";

      it->second->write_to_(os);

      for (++it; it != std::cend(this->object()); ++it) {
        os << ",\"" << it->first << "\":";

        it->second->write_to_(os);
      }
    }

    os << '}';

    break;
  case Type::array:
    os << '[';

    if (!this->array().empty()) {
      auto it = std::cbegin(this->array());

      it->write_to_(os);

      for (++it; it < std::cend(this->array()); ++it) {
        os << ',';

        it->write_to_(os);
      }
    }

    os << ']';

    break;
  case Type::string:
    os << std::quoted(this->string());
    break;
  case Type::number:
    os << this->number();

    break;
  case Type::boolean:
    os << (this->boolean() ? "true" : "false");

    break;
  case Type::null:
    os << "null";

    break;
  }
}

auto Value::write_to_formatted_(std::ostream &os, const std::size_t current_indent, const std::size_t indent_size) const
    -> void {
  switch (this->type()) {
  case Type::object:
    os << "{\n";

    if (!this->object().empty()) {
      auto it = std::cbegin(this->object());

      stream::write_spaces_to(os, current_indent + indent_size);

      os << '"' << it->first << "\" : ";

      it->second->write_to_formatted_(os, current_indent + indent_size, indent_size);

      for (++it; it != std::cend(this->object()); ++it) {
        os << ",\n";

        stream::write_spaces_to(os, current_indent + indent_size);

        os << '"' << it->first << "\" : ";

        it->second->write_to_formatted_(os, current_indent + indent_size, indent_size);
      }
    }

    os << '\n';

    stream::write_spaces_to(os, current_indent);

    os << '}';

    break;
  case Type::array:
    os << "[\n";

    if (!this->array().empty()) {
      auto it = std::cbegin(this->array());

      stream::write_spaces_to(os, current_indent + indent_size);

      it->write_to_formatted_(os, current_indent + indent_size, indent_size);

      for (++it; it < std::cend(this->array()); ++it) {
        os << ",\n";

        stream::write_spaces_to(os, current_indent + indent_size);

        it->write_to_formatted_(os, current_indent + indent_size, indent_size);
      }
    }

    os << '\n';

    stream::write_spaces_to(os, current_indent);

    os << ']';

    break;
  case Type::string:
    os << std::quoted(this->string());

    break;
  case Type::number:
    os << this->number();

    break;
  case Type::boolean:
    os << (this->boolean() ? "true" : "false");

    break;
  case Type::null:
    os << "null";

    break;
  }
}

auto operator==(const Value &left, const Value &right) -> bool {
  if (left.type() != right.type()) {
    return false;
  }

  switch (left.type()) {
  case Type::object:
    return left.object() == right.object();
  case Type::array:
    return left.array() == right.array();
  case Type::string:
    return left.string() == right.string();
  case Type::number:
    return math::equals(left.number(), right.number());
  case Type::boolean:
    return left.boolean() == right.boolean();
  case Type::null:
    return true;
  }

  return false;
}

auto operator!=(const Value &left, const Value &right) -> bool { return !(left == right); }

auto operator<<(std::ostream &os, const Value &value) -> std::ostream & {
  value.write_to(os);

  return os;
}
} // namespace cw::json

namespace cw::json::parse {
Parser::~Parser() = default;

auto Parser::object_reserve(const std::size_t size) noexcept -> Parser & {
  this->object_reserve_ = size;

  return *this;
}

auto Parser::array_reserve(const std::size_t size) noexcept -> Parser & {
  this->array_reserve_ = size;

  return *this;
}

auto Parser::string_reserve(const std::size_t size) noexcept -> Parser & {
  this->string_reserve_ = size;

  return *this;
}

auto Parser::stream__() const -> const std::unique_ptr<std::istream> & { return this->stream_; }

auto Parser::stream__() -> std::unique_ptr<std::istream> & { return this->stream_; }

auto Parser::parse_whitespace_() -> void {
  while (std::isspace(this->stream__()->peek()) != 0) {
    this->stream__()->ignore();
  }
}

auto Parser::parse_value_() -> Value {
  if (this->stream__()->peek() == '{') {
    return this->parse_object_();
  }

  if (this->stream__()->peek() == '[') {
    return this->parse_array_();
  }

  if (this->stream__()->peek() == '"') {
    return this->parse_string_();
  }

  if (this->stream__()->peek() == '-' || std::isdigit(this->stream__()->peek()) != 0) {
    return this->parse_number_();
  }

  if (this->stream__()->peek() == 't' || this->stream__()->peek() == 'T' || this->stream__()->peek() == 'f' ||
      this->stream__()->peek() == 'F') {
    return this->parse_boolean_();
  }

  if (this->stream__()->peek() == 'n' || this->stream__()->peek() == 'N') {
    return this->parse_null_();
  }

  throw Error(stream::line_and_column(*this->stream__()), static_cast<char>(this->stream__()->peek()));
}

auto Parser::parse_key_() -> std::string {
  auto string = json::String("");

  string.reserve(this->string_reserve_);

  this->stream__()->ignore();

  while (this->stream__()->peek() != '"' && !this->stream__()->eof()) {
    if (this->stream__()->peek() == '\\') {
      string.push_back(static_cast<char>(this->stream__()->get()));
    }

    if (this->stream__()->peek() == '\r' || this->stream__()->peek() == '\n') {
      throw Error(stream::line_and_column(*this->stream__()), "Key is not terminated properly.");
    }

    string.push_back(static_cast<char>(this->stream__()->get()));
  }

  this->stream__()->ignore();

  this->parse_whitespace_();

  return string;
}

auto Parser::parse_object_() -> Value {
  auto object = Object();

  object.reserve(this->object_reserve_);

  this->stream__()->ignore();

  while (this->stream__()->peek() != '}' && !this->stream__()->eof()) {
    this->parse_whitespace_();

    if (this->stream__()->peek() != '"') {
      throw Error(stream::line_and_column(*this->stream__()), ':', static_cast<char>(this->stream__()->peek()));
    }

    auto key = this->parse_key_();

    if (this->stream__()->peek() != ':') {
      throw Error(stream::line_and_column(*this->stream__()), ':', static_cast<char>(this->stream__()->peek()));
    }

    this->stream__()->ignore();

    this->parse_whitespace_();

    auto value = this->parse_value_();

    object.emplace(std::make_pair(std::move(key), std::make_unique<Value>(std::move(value))));

    if (this->stream__()->peek() != ',' && this->stream__()->peek() != '}') {
      throw Error(stream::line_and_column(*this->stream__()), static_cast<char>(this->stream__()->peek()));
    }

    if (this->stream__()->peek() == ',') {
      this->stream__()->ignore();
    }

    this->parse_whitespace_();
  }

  this->stream__()->ignore();

  this->parse_whitespace_();

  return Value(std::move(object));
}

auto Parser::parse_array_() -> Value {
  auto array = Array();

  array.reserve(this->array_reserve_);

  this->stream__()->ignore();

  while (this->stream__()->peek() != ']' && !this->stream__()->eof()) {
    this->parse_whitespace_();

    auto value = this->parse_value_();

    array.emplace_back(std::move(value));

    if (this->stream__()->peek() != ',' && this->stream__()->peek() != ']') {
      throw Error(stream::line_and_column(*this->stream__()), static_cast<char>(this->stream__()->peek()));
    }

    if (this->stream__()->peek() == ',') {
      this->stream__()->ignore();
    }

    this->parse_whitespace_();
  }

  this->stream__()->ignore();

  this->parse_whitespace_();

  return Value(std::move(array));
}

auto Parser::parse_string_() -> Value {
  auto string = json::String("");

  string.reserve(this->string_reserve_);

  this->stream__()->ignore();

  while (this->stream__()->peek() != '"' && !this->stream__()->eof()) {
    if (this->stream__()->peek() == '\\') {
      string.push_back(static_cast<char>(this->stream__()->get()));
    }

    if (this->stream__()->peek() == '\r' || this->stream__()->peek() == '\n') {
      throw Error(stream::line_and_column(*this->stream__()), "String is not terminated properly.");
    }

    string.push_back(static_cast<char>(this->stream__()->get()));
  }

  this->stream__()->ignore();

  this->parse_whitespace_();

  return Value(std::move(string));
}

auto Parser::parse_number_() -> Value {
  auto number{0.0};

  (*this->stream__()) >> number;

  this->parse_whitespace_();

  return Value(number);
}

auto Parser::parse_boolean_() -> Value {
  if (this->stream__()->peek() == 't' || this->stream__()->peek() == 'T') {
    this->stream__()->ignore();

    if (this->stream__()->peek() != 'r' && this->stream__()->peek() != 'R') {
      throw Error(stream::line_and_column(*this->stream__()), static_cast<char>(this->stream__()->peek()));
    }

    this->stream__()->ignore();

    if (this->stream__()->peek() != 'u' && this->stream__()->peek() != 'U') {
      throw Error(stream::line_and_column(*this->stream__()), static_cast<char>(this->stream__()->peek()));
    }

    this->stream__()->ignore();

    if (this->stream__()->peek() != 'e' && this->stream__()->peek() != 'E') {
      throw Error(stream::line_and_column(*this->stream__()), static_cast<char>(this->stream__()->peek()));
    }

    this->stream__()->ignore();

    this->parse_whitespace_();

    return Value(true);
  }

  this->stream__()->ignore();

  if (this->stream__()->peek() != 'a' && this->stream__()->peek() != 'A') {
    throw Error(stream::line_and_column(*this->stream__()), static_cast<char>(this->stream__()->peek()));
  }

  this->stream__()->ignore();

  if (this->stream__()->peek() != 'l' && this->stream__()->peek() != 'L') {
    throw Error(stream::line_and_column(*this->stream__()), static_cast<char>(this->stream__()->peek()));
  }

  this->stream__()->ignore();

  if (this->stream__()->peek() != 's' && this->stream__()->peek() != 'S') {
    throw Error(stream::line_and_column(*this->stream__()), static_cast<char>(this->stream__()->peek()));
  }

  this->stream__()->ignore();

  if (this->stream__()->peek() != 'e' && this->stream__()->peek() != 'E') {
    throw Error(stream::line_and_column(*this->stream__()), static_cast<char>(this->stream__()->peek()));
  }

  this->stream__()->ignore();

  this->parse_whitespace_();

  return Value(false);
}

auto Parser::parse_null_() -> Value {
  this->stream__()->ignore();

  if (this->stream__()->peek() != 'u' && this->stream__()->peek() != 'U') {
    throw Error(stream::line_and_column(*this->stream__()), static_cast<char>(this->stream__()->peek()));
  }

  this->stream__()->ignore();

  if (this->stream__()->peek() != 'l' && this->stream__()->peek() != 'L') {
    throw Error(stream::line_and_column(*this->stream__()), static_cast<char>(this->stream__()->peek()));
  }

  this->stream__()->ignore();

  if (this->stream__()->peek() != 'l' && this->stream__()->peek() != 'L') {
    throw Error(stream::line_and_column(*this->stream__()), static_cast<char>(this->stream__()->peek()));
  }

  this->stream__()->ignore();

  this->parse_whitespace_();

  return Value();
}

File::File(std::string filename) : filename_(std::move(filename)) {}

File::~File() = default;

auto File::buffer_size(const std::size_t size) noexcept -> File & {
  this->buffer_size_ = size;

  return *this;
}

auto File::parse() -> Value {
  auto ifs = std::ifstream();

  ifs.exceptions(std::ifstream::failbit | std::ifstream::badbit);

  auto buffer = std::vector<char>(this->buffer_size_, '\0');

  ifs.rdbuf()->pubsetbuf(buffer.data(), static_cast<std::streamsize>(this->buffer_size_));

  try {
    ifs.open(this->filename_, std::ifstream::in);
  } catch (const std::ios_base::failure &) {
    throw Error(string::Builder("Unable to open file '").append(this->filename_).append("' for reading.").build());
  }

  try {
    this->stream__() = std::make_unique<std::ifstream>(std::move(ifs));

    this->parse_whitespace_();

    return this->parse_value_();
  } catch (const std::ios_base::failure &) {
    if (this->stream__()->eof()) {
      throw Error("Unexpected end of file.");
    }

    throw Error("Unexpected file I/O error.");
  }
}

String::String(const std::string &value) {
  this->stream__() = std::make_unique<std::istringstream>(std::istringstream(value));
}

String::~String() = default;

auto String::parse() -> Value {
  this->stream__()->exceptions(std::ifstream::failbit | std::ifstream::badbit);

  try {
    this->parse_whitespace_();

    return this->parse_value_();
  } catch (const std::ios_base::failure &) {
    if (this->stream__()->eof()) {
      throw Error("Unexpected end of string.");
    }

    throw Error("Unexpected string I/O error.");
  }
}
} // namespace cw::json::parse

namespace cw::json::writer {
File::File(std::string filename) : filename_(std::move(filename)) {}

auto File::buffer_size(const std::size_t size) noexcept -> File & {
  this->buffer_size_ = size;

  return *this;
}

auto File::formatted(const bool value) noexcept -> File & {
  this->formatted_ = value;

  return *this;
}

auto File::write(const Value &value) const -> void {
  auto ofs = std::ofstream();

  ofs.exceptions(std::ofstream::failbit | std::ofstream::badbit);

  auto buffer = std::vector<char>(this->buffer_size_, '\0');

  ofs.rdbuf()->pubsetbuf(buffer.data(), static_cast<std::streamsize>(this->buffer_size_));

  try {
    ofs.open(this->filename_, std::ofstream::out);
  } catch (const std::ios_base::failure &) {
    throw Error(string::Builder("Unable to open file '").append(this->filename_).append("' for writing.").build());
  }

  try {
    if (this->formatted_) {
      value.write_to_formatted(ofs);
    } else {
      value.write_to(ofs);
    }
  } catch (const std::ios_base::failure &) {
    throw Error("Unexpected file I/O error.");
  }
}
} // namespace cw::json::writer

namespace cw::json::builder {
Object::Object(const std::string &key) { this->object_.emplace(std::make_pair(key, std::make_unique<Value>(Value()))); }

Object::Object(std::string &&key) {
  this->object_.emplace(std::make_pair(std::move(key), std::make_unique<Value>(Value())));
}

Object::Object(const char *const key) {
  this->object_.emplace(std::make_pair(std::string(key), std::make_unique<Value>(Value())));
}

Object::Object(const std::string &key, const char *const value) {
  this->object_.emplace(std::make_pair(key, std::make_unique<Value>(Value(String(value)))));
}

Object::Object(std::string &&key, const char *const value) {
  this->object_.emplace(std::make_pair(std::move(key), std::make_unique<Value>(Value(String(value)))));
}

Object::Object(const char *const key, const char *const value) {
  this->object_.emplace(std::make_pair(std::string(key), std::make_unique<Value>(Value(String(value)))));
}

Object::Object(const Object &object) {
  auto new_object = json::Object();

  for (const auto &entry : object.object_) {
    new_object[entry.first] = std::make_unique<Value>(*entry.second);
  }

  this->object_ = std::move(new_object);
}

auto Object::operator=(const Object &object) -> Object & {
  if (this == &object) {
    return *this;
  }

  auto new_object = json::Object();

  for (const auto &entry : object.object_) {
    new_object[entry.first] = std::make_unique<Value>(*entry.second);
  }

  this->object_ = std::move(new_object);

  return *this;
}

auto Object::add(const std::string &key, const char *const value) -> Object & {
  this->object_.emplace(std::make_pair(key, std::make_unique<Value>(Value(String(value)))));

  return *this;
}

auto Object::add(std::string &&key, const char *const value) -> Object & {
  this->object_.emplace(std::make_pair(std::move(key), std::make_unique<Value>(Value(String(value)))));

  return *this;
}

auto Object::add(const char *const key, const char *const value) -> Object & {
  this->object_.emplace(std::make_pair(std::string(key), std::make_unique<Value>(Value(String(value)))));

  return *this;
}

auto Object::add(const std::string &key) -> Object & {
  this->object_.emplace(std::make_pair(key, std::make_unique<Value>(Value())));

  return *this;
}

auto Object::add(std::string &&key) -> Object & {
  this->object_.emplace(std::make_pair(std::move(key), std::make_unique<Value>(Value())));

  return *this;
}

auto Object::add(const char *const key) -> Object & {
  this->object_.emplace(std::make_pair(std::string(key), std::make_unique<Value>(Value())));

  return *this;
}

auto Object::build() -> json::Object {
  auto object = json::Object();

  for (const auto &entry : this->object_) {
    object[entry.first] = std::make_unique<Value>(*entry.second);
  }

  return object;
}

Array::Array(const char *const value) { this->array_.emplace_back(Value(std::string(value))); }

auto Array::add(const char *const value) -> Array & {
  this->array_.emplace_back(Value(std::string(value)));

  return *this;
}

auto Array::add() -> Array & {
  this->array_.emplace_back(Value());

  return *this;
}

auto Array::build() -> json::Array { return this->array_; }
} // namespace cw::json::builder
