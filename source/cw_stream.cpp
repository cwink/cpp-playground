#include "../include/cw_stream.hpp"

namespace cw::stream {
auto write_spaces_to(std::ostream &os, const std::size_t count) -> void {
  for (auto i{0UL}; i < count; ++i) {
    os << ' ';
  }
}

auto line_and_column(std::istream &is) -> Location {
  const auto current_position = is.tellg();

  is.seekg(0);

  auto line = 1;

  auto line_position{static_cast<std::streamoff>(0)};

  auto column = 0UL;

  for (auto ch = is.get(); is.tellg() != current_position && !is.eof(); ch = is.get()) {
    if (ch == '\r' && is.peek() == '\n') {
      is.ignore();

      ++line;

      line_position = is.tellg();

      continue;
    }

    if (ch == '\r' || ch == '\n') {
      ++line;

      line_position = is.tellg();
    }
  }

  column = static_cast<std::size_t>(is.tellg()) - static_cast<std::size_t>(line_position) + 1UL;

  is.seekg(current_position);

  return std::make_pair(line, column);
}
} // namespace cw::stream
