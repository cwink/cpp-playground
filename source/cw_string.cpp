#include "../include/cw_string.hpp"

namespace cw::string {
Builder::Builder() { this->oss_.exceptions(std::ostringstream::failbit | std::ostringstream::badbit); }

Builder::Builder(const char *const c_string) {
  this->oss_.exceptions(std::ostringstream::failbit | std::ostringstream::badbit);

  this->oss_ << c_string;
}

Builder::Builder(const Builder &builder) {
  this->oss_.exceptions(std::ostringstream::failbit | std::ostringstream::badbit);

  this->oss_ << builder.oss_.str();
}

auto Builder::operator=(const Builder &builder) -> Builder & {
  if (this == &builder) {
    return *this;
  }

  this->oss_.exceptions(std::ostringstream::failbit | std::ostringstream::badbit);

  this->oss_ << builder.oss_.str();

  return *this;
}

auto Builder::append(const char *const c_string) -> Builder & {
  this->oss_ << c_string;

  return *this;
}

auto Builder::prepend(const char *const c_string) -> Builder & {
  auto temp = std::ostringstream();

  temp << c_string << this->oss_.rdbuf();

  this->oss_ = std::move(temp);

  return *this;
}

auto Builder::build() -> std::string { return this->oss_.str(); }
} // namespace cw::string
