#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

auto factorial(std::uint32_t number) -> std::uint32_t;

auto factorial(const std::uint32_t number) -> std::uint32_t {
  return number <= 1 ? number : factorial(number - 1) * number;
}

SCENARIO("When a factorial is computed, it is the correct number.", "[factorial_calculations]") {
  GIVEN("Three factorial values.") {
    auto zero = factorial(0);
    auto one = factorial(1);
    auto two = factorial(2);
    auto three = factorial(3);

    WHEN("We test these values for correctness.") {
      THEN("A factorial of 0 is 1.") { REQUIRE(zero == 1); }

      THEN("A factorial of 1 is 1.") { REQUIRE(one == 1); }

      THEN("A factorial of 2 is 2.") { REQUIRE(two == 2); }

      THEN("A factorial of 3 is 6.") { REQUIRE(three == 6); }
    }
  }
}
